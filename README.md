This is a small Gemini server in PHP, made to be extended by your own code.

There is a Files\RequestHandler that you can use to serve files.

See gemini-example.php for an example how to set it up.

If you do not want to write PHP code you can use gemini-server.php and pass it a JSON configuration file which indicates handlers configuration.
See config.json for an example.

Not supported (may be some day):
- Client certificates
- Session handling
- Input
