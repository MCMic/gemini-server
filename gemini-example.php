<?php

require __DIR__ . '/vendor/autoload.php';

use MCMic\Gemini;

$server = new Gemini\Server();

$server->addHandler(
    new Gemini\Files\RequestHandler('myserver.example.com', __DIR__ . '/example.com-cert.pem', __DIR__ . '/root', ['*.gmi'])
);

$server->serve();
