#!/bin/php
<?php

require __DIR__ . '/vendor/autoload.php';

use MCMic\Gemini;

if (!isset($argv[1])) {
    echo "Usage: $argv[0] <csvfile>\n";
    die();
}

$containers = Gemini\Statistics\Container::computeData($argv[1]);
foreach ($containers as $label => $container) {
    if (!$container->isEmpty()) {
        echo '# ' . $label . "\r\n";
        echo $container->render();
    }
}
