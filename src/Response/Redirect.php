<?php

declare(strict_types=1);

namespace MCMic\Gemini\Response;

use MCMic\Gemini;

class Redirect extends Gemini\Response
{
    public function __construct(string $uri)
    {
        parent::__construct(30, Gemini\Request::urlEncode($uri));
    }
}
