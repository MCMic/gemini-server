<?php

declare(strict_types=1);

namespace MCMic\Gemini\Response;

use MCMic\Gemini;

class Input extends Gemini\Response
{
    public function __construct(string $meta)
    {
        parent::__construct(10, $meta);
    }
}
