<?php

declare(strict_types=1);

namespace MCMic\Gemini\Response;

use MCMic\Gemini;

class Success extends Gemini\Response
{
    public function __construct(string $body = '', string $mimetype = 'text/gemini', string $lang = '', string $charset = '')
    {
        if ($charset !== '') {
            $mimetype .= '; charset=' . $charset;
        }
        if ($lang !== '') {
            $mimetype .= '; lang=' . $lang;
        }
        parent::__construct(20, $mimetype, $body);
    }
}
