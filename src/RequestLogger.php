<?php

declare(strict_types=1);

namespace MCMic\Gemini;

use Psr\Log;

class RequestLogger implements Log\LoggerAwareInterface
{
    public Log\LoggerInterface $logger;

    public function __construct(?Log\LoggerInterface $logger = null)
    {
        $this->setLogger($logger ?? new StdoutLogger());
    }

    public function setLogger(Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    protected function hashPeername(string $peername): string
    {
        return sha1(preg_replace('/:[0-9]+$/', '', $peername) ?? $peername);
    }

    public function logRequest(string $peername, string $request): void
    {
        $this->logger->info(static::hashPeername($peername) . ' Request: ' . trim($request));
    }

    /**
     * @param string|Request $request
     */
    public function logException(string $peername, $request, Exception $e): void
    {
        $this->logger->notice(static::hashPeername($peername) . ' Error:' . trim((string)$e));
    }

    public function logResponse(string $peername, Request $request, Response $response): void
    {
    }
}
