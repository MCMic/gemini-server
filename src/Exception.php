<?php

declare(strict_types=1);

namespace MCMic\Gemini;

class Exception extends \Exception
{
    public function __toString(): string
    {
        return $this->getCode() . ' ' . $this->getMessage() . "\r\n";
    }
}
