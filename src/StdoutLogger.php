<?php

declare(strict_types=1);

namespace MCMic\Gemini;

/**
 * Log to stdout
 */
class StdoutLogger extends \Psr\Log\AbstractLogger
{
    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array<mixed> $context
     *
     * @return void
     *
     * @throws \Psr\Log\InvalidArgumentException
     */
    public function log($level, $message, array $context = array())
    {
        if (is_string($level)) {
            echo '[' . strtoupper($level) . '] ';
        }
        echo $message . "\r\n";
    }
}
