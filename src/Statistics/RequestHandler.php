<?php

declare(strict_types=1);

namespace MCMic\Gemini\Statistics;

use MCMic\Gemini;

class RequestHandler extends Gemini\RequestHandler
{
    /**
     * @var string
     */
    protected $filePath;

    public function __construct(string $host, string $certPath, string $filePath)
    {
        parent::__construct($host, $certPath);

        $this->filePath = $filePath;
    }

    public function handle(Gemini\Request $request): Gemini\Response
    {
        $output = '';

        $containers = Gemini\Statistics\Container::computeData($this->filePath);
        foreach ($containers as $label => $container) {
            if (!$container->isEmpty()) {
                $output .= '# ' . $label . "\r\n";
                $output .= $container->render();
            }
        }

        return new Gemini\Response\Success($output, 'text/gemini', 'en');
    }
}
