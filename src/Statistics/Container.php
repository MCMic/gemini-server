<?php

declare(strict_types=1);

namespace MCMic\Gemini\Statistics;

class Container
{
    /**
     * @var array<string,int>
     */
    public $visits = [];
    /**
     * @var array<string,int>
     */
    public $pageviews = [];
    /**
     * @var array<string,array<string,int>>
     */
    public $errors = [];
    /**
     * @var ?\Datetime
     */
    public $date = null;

    public function __construct(?string $date = null)
    {
        if ($date !== null) {
            $this->date = new \DateTime($date);
        }
    }

    /**
     * @param array<string> $data
     */
    public function append(array $data): bool
    {
        [$date, $id, $request, $status, $meta] = $data;
        if (isset($this->date)) {
            $date = new \DateTime($date);
            if ($date < $this->date) {
                return false;
            }
        }
        if (!isset($this->visits[$id])) {
            $this->visits[$id] = 0;
        }
        $this->visits[$id]++;
        if ($status == 20) {
            if (!isset($this->pageviews[$request])) {
                $this->pageviews[$request] = 0;
            }
            $this->pageviews[$request]++;
        } elseif ($status >= 40) {
            if (!isset($this->errors[$request][$status . ' ' . $meta])) {
                $this->errors[$request][$status . ' ' . $meta] = 0;
            }
            $this->errors[$request][$status . ' ' . $meta]++;
        }
        return true;
    }

    public function isEmpty(): bool
    {
        return (count($this->visits) + count($this->pageviews) + count($this->errors) === 0);
    }

    public function render(): string
    {
        $output = '## Visits' . "\r\n";
        $output .= count($this->visits) . ' unique visitors' . "\r\n";
        $output .= array_sum($this->pageviews) . ' page hits' . "\r\n";

        if (count($this->pageviews) > 0) {
            arsort($this->pageviews);
            reset($this->pageviews);
            for ($i = 0; $i <= 5; $i++) {
                $output .= '* ' . key($this->pageviews) . ': ' . current($this->pageviews) . "\r\n";
                if (next($this->pageviews) === false) {
                    break;
                }
            }
        }

        $output .= '## Errors' . "\r\n";
        $output .= array_sum(array_map('array_sum', $this->errors)) . ' errors' . "\r\n";
        if (count($this->errors) > 0) {
            uasort(
                $this->errors,
                function ($a, $b): int {
                    return array_sum($b) <=> array_sum($a);
                }
            );
            reset($this->errors);
            for ($i = 0; $i <= 5; $i++) {
                $output .= key($this->errors) . ': ' . array_sum(current($this->errors)) . "\r\n";
                foreach (current($this->errors) as $error => $count) {
                    $output .= '* ' . $error . ': ' . $count . "\r\n";
                }
                if (next($this->errors) === false) {
                    break;
                }
            }
        }
        return $output;
    }

    /**
     * @return array<string,Container>
     */
    public static function computeData(string $path): array
    {
        $containers = [
            'Today' => new Container('today'),
            'Last 7 days' => new Container('7 days ago'),
            'This month' => new Container('first day of this month'),
            'Total' => new Container(),
        ];

        if (($handle = fopen($path, 'r')) !== false) {
            while (($data = fgetcsv($handle, 2000, ',')) !== false) {
                foreach ($containers as $container) {
                    $container->append($data);
                }
            }
            fclose($handle);
        }
        return $containers;
    }
}
