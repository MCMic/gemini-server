<?php

declare(strict_types=1);

namespace MCMic\Gemini;

abstract class RequestHandler
{
    public string $host;
    public string $certPath;
    /**
     * @var array<Response>
     */
    public array $staticPaths = [];

    public function __construct(string $host, string $certPath)
    {
        $this->host = $host;
        $this->certPath = $certPath;
    }

    public function handleWrapper(Request $request): Response
    {
        foreach ($this->staticPaths as $path => $response) {
            if ($path === ($request->path ?? '')) {
                return $response;
            }
        }
        return $this->handle($request);
    }

    abstract public function handle(Request $request): Response;

    public function setFavicon(string $emoji): void
    {
        $this->staticPaths['/favicon.txt'] = new Response\Success($emoji, 'text/plain');
    }

    public function setRobotsRules(string $content): void
    {
        $this->staticPaths['/robots.txt'] = new Response\Success($content, 'text/plain');
    }
}
