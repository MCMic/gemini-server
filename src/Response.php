<?php

declare(strict_types=1);

namespace MCMic\Gemini;

class Response
{
    public int $status;
    public string $meta;
    public ?string $body;

    /**
     * @throws \MCMic\Gemini\Exception
     */
    public function __construct(int $status, string $meta, ?string $body = null)
    {
        if (
            (($status < 20) || ($status >= 30)) &&
            ($body !== null)
        ) {
            throw new Exception('Response with status ' . $status . ' cannot have a body', 40);
        }

        $this->status   = $status;
        $this->meta     = $meta;
        $this->body     = $body;
    }

    public function __toString(): string
    {
        return $this->status . ' ' . $this->meta . "\r\n" . ($this->body ?? '');
    }
}
