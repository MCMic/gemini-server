#!/bin/php
<?php

require __DIR__ . '/vendor/autoload.php';

use MCMic\Gemini;

if (isset($argv[1])) {
    $content = file_get_contents($argv[1]);
    if ($content === false) {
        throw new \Exception('Could not open ' . $argv[1]);
    }
    $config = json_decode($content, true, 10, JSON_THROW_ON_ERROR);
    $server = new Gemini\Server();
    foreach (($config['handlers'] ?? []) as $handler) {
        if (
            !isset($handler['class']) ||
            !isset($handler['domain']) ||
            !isset($handler['cert'])
        ) {
            throw new \Exception('Each handler in configuration file must have class/domain/cert keys');
        }
        $handlerObject = new $handler['class']($handler['domain'], realpath($handler['cert']), ...($handler['parameters'] ?? []));
        $handlerObject->staticPaths = array_map(
            function (array $params): Gemini\Response {
                return new Gemini\Response(...$params);
            },
            ($handler['staticPaths'] ?? [])
        );
        $server->addHandler($handlerObject);
    }
    $server->serve();
} else {
    echo "Usage: $argv[0] <configfile>\n";
}
